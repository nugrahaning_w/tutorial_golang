package main

import "fmt"

func main() {
	//array golang harus dideklarasi maksimal panjangnya
	var names [3]string
	names[0] = "Aji"
	names[1] = "Nugrahaning"
	names[2] = "Widhi"

	fmt.Println(names)
	fmt.Println(names[2])

	var i [100]int
	for n := 1; n <= len(i); n++ {
		i[n-1] = n
	}
	for k := 1; k <= len(i); k++ {
		fmt.Println(i[k-1])
		if k == 50 {
			break
		}
	}
	//foreach
	for n, value := range names {
		fmt.Println("index", n, "=", value)
	}
	for _, value := range names {
		fmt.Println(value)
	}
}
