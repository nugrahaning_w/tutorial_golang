package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		//print ganjil menggunakan if
		//if i%2 == 1{
		//	fmt.Println(i)
		//}

		//print ganjil menggunakan continue
		//if i%2==0 {
		//	continue
		//}
		//fmt.Println(i)

		//stop perulangan ketika i>50
		if i == 50 {
			break
		}
		fmt.Println(i)
	}
}
