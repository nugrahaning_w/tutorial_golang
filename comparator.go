package main

import "fmt"

func main() {
	a := 1
	b := 1

	//samadengan
	result := a == b
	fmt.Println(result)
	//tidak samadengan
	result = a != b
	fmt.Println(result)
	//lebih dari
	result = a > b
	println(result)
	//kurang dari
	result = a < b
	println(result)
	//lebih dari atau samadengan
	result = a >= b
	println(result)
	//kurang dari atau samadengan
	result = a <= b
	println(result)
}
