package main

import (
	"fmt"
	"strconv"
)

func main() {
	//float->int tanpa pembulatan
	var x float32 = 3.832
	var y int8 = int8(x)

	fmt.Println(x)
	fmt.Println(y)

	//konversi string -> int
	var nilai string = "100"
	//Atoi return 2 values (hasil,error)
	//membuang values error yang tidak terpakai
	nilaiInt, _ := strconv.Atoi(nilai)
	fmt.Println(nilaiInt)

	//konversi int -> string
	nilaiStr := strconv.Itoa(-100)
	fmt.Println(nilaiStr)
}
