package main

import "fmt"

func main() {
	//unlimited for
	//for {
	//	fmt.Println("Hallo for")
	//}
	//inisiasi variabel external
	i := 1
	for i <= 5 {
		fmt.Println(i, "external")
		i++
	}
	//inisiasi variabel internal
	for j := 1; j <= 5; j++ {
		fmt.Println(j, "internal")
	}
}
