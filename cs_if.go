package main

import "fmt"

func main() {
	//dual condition
	//for i:=1 ; i<=10 ; i++ {
	//	if i % 2 == 1 {
	//		fmt.Println(i, " Ganjil")
	//	}else{
	//		fmt.Println(i, " Genap")
	//	}
	//}
	//multi condition
	for i := 1; i <= 30; i++ {
		if i%3 == 0 {
			fmt.Println(i, "Foo")
		} else if i%5 == 0 {
			fmt.Println(i, "Bar")
		} else if i%3 == 0 && i%5 == 0 {
			fmt.Println(i, "FooBar")
			//	kondisi foobar ke skip karena sudah kehandle kondisi foo atau bar sebelum masuk kondisi ini
			//harus dipindah paling atas karena for mengeksekusi kondisi sesuai urutan
		} else {
			fmt.Println(i)
		}
	}
}
