package main

import (
	"fmt"
	"runtime"
)

func main() {

	for i := 1; i <= 10; i++ {
		//CS if
		//if i == 1 {
		//	fmt.Println("Satu")
		//}else if i == 2 {
		//	fmt.Println("Dua")
		//}else if i == 3 {
		//	fmt.Println("Tiga")
		//}else if i == 4 {
		//	fmt.Println("Empat")
		//}else if i == 5 {
		//	fmt.Println("Lima")
		//}else {
		//	fmt.Println("Tidak Tahu")
		//}

		//switch integer
		switch i {
		case 1:
			fmt.Println("One")
		case 2:
			fmt.Println("Two")
		case 3:
			fmt.Println("Three")
		case 4:
			fmt.Println("Four")
		case 5:
			fmt.Println("Five")
		default:
			fmt.Println("Unknown")
		}
	}
	//switch string
	//GOOS = read device os in go
	os := runtime.GOOS
	switch os {
	case "darwin":
		fmt.Println("Mac")
	case "linux":
		fmt.Println("Linux")
	case "windows":
		fmt.Println("Windows 10")
	default:
		fmt.Println("Unknown Operating Sistem")

	}

}
