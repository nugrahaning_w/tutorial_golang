package main

import "fmt"

func main() {
	//number
	//uint8,uint16,uint32,uint64/byte
	fmt.Println(1)
	//int8,int16,int32,int64
	fmt.Println(-2)
	//float32,float64/complex
	fmt.Println(1.1)
	//string
	fmt.Println("Aji")
	fmt.Println("Aji Nugrahaning Widhi")
	//boolean
	fmt.Println(true)
	fmt.Println(false)
	fmt.Println(true || false)
	fmt.Println(true && false)
}
