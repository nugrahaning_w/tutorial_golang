package main

import "fmt"

func main() {
	fmt.Println(1 + 1)
	fmt.Println(10 - 1)
	fmt.Println(10 * 2)
	//float/float=float
	fmt.Println(10.0 / 3.0)
	//int/int=int
	fmt.Println(10 / 3)
	fmt.Println(10 % 3)
	var a = 1
	var b = 2
	if a == b {
		fmt.Println("Benar")
	} else {
		fmt.Println("Salah")
	}
}
