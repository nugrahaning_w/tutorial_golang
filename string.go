package main

import "fmt"

func main() {
	fmt.Println("Belajar Golang")
	//Penggabungan string
	fmt.Println("Belajar" + " " + "Golang")
	//Mengambil panjang String
	fmt.Println(len("Belajar"))
	//Mengambil Byte karakter pertama (index ke 0)
	fmt.Println("Belajar"[0])
	//Mengambil karakter pertama sampai 4
	fmt.Println("Belajar"[0:4])
	fmt.Println("Belajar"[:4])
	//mengambil karakter keempat sampai terakhir
	fmt.Println("Belajar"[3:])
}
