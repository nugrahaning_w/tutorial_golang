package main

import "fmt"

func main() {
	//replace variable
	var huruf string
	fmt.Println(huruf)
	huruf = "Hallo world"
	fmt.Println(huruf)
	huruf = "Belajar Golang"
	fmt.Println(huruf)

	//membuat variabel string tanpa deklarasi type data
	tanpatype := "Tanpa declare type variable"
	fmt.Println(tanpatype)

	//membuat variable konstan (tidak dapat di replace)
	const perusahaan string = "Sonarum.io"
	fmt.Println(perusahaan)
}
